import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../service/app.service';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private router:Router,private service:AppService,private httpClientService: HttpClientService) { }

  ngOnInit() {

  }
goToLogin()
{
this.router.navigate(['/login']);
}
goToSignup()
{
this.router.navigate(['/signup']);
}
logout()
  {
  sessionStorage.removeItem('token');
  this.service.isLoggedIn(false);
  this.router.navigate(['login']);
  }
  goToMyProfile()
  {
  this.router.navigate(['/my-profile']);
  }
   goToHome()
   {
   this.router.navigate(['/home']);
   }
   goToViewBlogs()
   {
   this.router.navigate(['/view-blogs']);
   }
   goToCreateBlog()
   {
     this.router.navigate(['/create-blog']);
   }
}

