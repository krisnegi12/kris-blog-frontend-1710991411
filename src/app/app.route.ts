import {Routes} from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {MyProfileComponent} from './my-profile/my-profile.component';
import {ViewBlogsComponent} from './view-blogs/view-blogs.component';
import {BlogDetailComponent} from './blog-detail/blog-detail.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {CreateBlogComponent} from './create-blog/create-blog.component';
import {UpdateBlogComponent} from './update-blog/update-blog.component';

export const MAIN_ROUTES: Routes = [
{
path:'',
redirectTo:'home',
pathMatch:'full',
},
{
path:'login',
component:LoginComponent
},
{
path:'signup',
component:SignupComponent
},
{
path:'home',
component:HomePageComponent
},
{
path:'my-profile',
component:MyProfileComponent
},
{
path:'view-blogs',
component:ViewBlogsComponent
},
{
path:'blog-detail',
component:BlogDetailComponent
},
{
path:'user-profile',
component:UserProfileComponent
},
{
path:'create-blog',
component:CreateBlogComponent
},
{
path:'update-blog',
component:UpdateBlogComponent
},
{
path:'**',
redirectTo:'home'
}
];
