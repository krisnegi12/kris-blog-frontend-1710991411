import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { HttpClientModule } from '@angular/common/http';
import {HttpClientService} from './service/http-client.service';
import {AppService} from './service/app.service';
import {AuthenticationService} from './service/authentication.service';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { UpdateBlogComponent } from './update-blog/update-blog.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ViewBlogsComponent } from './view-blogs/view-blogs.component';
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavBarComponent,
    LoginComponent,
    SignupComponent,
    MyProfileComponent,
    BlogDetailComponent,
    CreateBlogComponent,
    UpdateBlogComponent,
    UserProfileComponent,
    ViewBlogsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClientService,AppService,AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
