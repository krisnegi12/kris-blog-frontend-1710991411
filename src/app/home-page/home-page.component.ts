import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AppService} from '../service/app.service';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
blogs;
searchbar;

  constructor(private router:Router,private service:AppService,private httpClientService: HttpClientService) { }

  ngOnInit() {
  if(!this.service.checklogin())
  {
  this.router.navigate(['login']);
  }
  this.httpClientService.getNewsFeed().subscribe(
         response =>{this.blogs = response;}
        );
  }
  goToBlogDetail(id)
  {
      this.router.navigate(['/blog-detail'],{queryParams:{id:id}});
  }
  getProperDate(milli)
  {
    var d=new Date(milli);
    return d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
  }
  goToViewBlogs()
  {
     this.router.navigate(['/view-blogs']);
  }
  goToHome()
  {
     this.router.navigate(['/home']);
  }
  goToCreateBlog()
  {
  this.router.navigate(['/create-blog']);
  }
  getBlogBySearch()
    {
    this.httpClientService.getBlogBySearch(this.searchbar).subscribe(
               response =>{this.blogs = response;}
              );
    }
    getBlogByAnytime()
    {
    this.httpClientService.getAllBlogs().subscribe(
               response =>{this.blogs = response;}
              );
    }
    getBlogByLastHour()
    {
      this.httpClientService.getBlogByLastHour().subscribe(
                 response =>{this.blogs = response;}
                );
    }
    getBlogByToday()
    {
        this.httpClientService.getBlogByToday().subscribe(
                   response =>{this.blogs = response;}
                  );
    }
    getBlogByThisWeek()
    {
          this.httpClientService.getBlogByThisWeek().subscribe(
                     response =>{this.blogs = response;}
                    );
    }
    getBlogByThisMonth()
    {
          this.httpClientService.getBlogByThisMonth().subscribe(
                     response =>{this.blogs = response;}
                    );
    }
    getBlogByThisYear()
    {
          this.httpClientService.getBlogByThisYear().subscribe(
                     response =>{this.blogs = response;}
                    );
    }
    logout()
    {
      sessionStorage.removeItem('token');
      this.service.isLoggedIn(false);
      this.router.navigate(['login']);
    }
    goToMyProfile()
    {
      this.router.navigate(['/my-profile']);
    }
}
