import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClientService} from '../service/http-client.service';
import {AppService} from '../service/app.service';

@Component({
  selector: 'app-view-blogs',
  templateUrl: './view-blogs.component.html',
  styleUrls: ['./view-blogs.component.scss']
})
export class ViewBlogsComponent implements OnInit {
blogs;
user;
searchbar;
following:any=[];

  constructor(private router:Router,private httpClientService: HttpClientService,private service:AppService) { }

  ngOnInit() {
  this.httpClientService.getAllBlogs().subscribe(
           response =>{this.blogs = response;}
          );
  this.httpClientService.getUser().subscribe(response=>{this.user=response;
  this.httpClientService.getUsersFollowing(this.user.id).subscribe(
                       response =>{this.following = response;}
                      );
  });
  }
  goToBlogDetail(id)
  {
    this.router.navigate(['/blog-detail'],{queryParams:{id:id}});
  }
  checkBlogAccess(b)
  {
    if(b.access==true)
    {
       if(b.user.id==this.user.id)
       {
          return true;
       }
       else
       {
          const found=this.following.find(function(value){
          return value.subscribed.id===b.user.id;
          });
          if(found)
          {
          return true;
          }
          else
          {
          return false;
          }
       }
    }
    else
    {
       return true;
    }
  }
  getProperDate(milli)
  {
      var d=new Date(milli);
      return d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
  }
  goToViewBlogs()
  {
     this.router.navigate(['/view-blogs']);
  }
  goToHome()
  {
     this.router.navigate(['/home']);
  }
  goToCreateBlog()
  {
    this.router.navigate(['/create-blog']);
  }
  getBlogBySearch()
  {
  this.httpClientService.getBlogBySearch(this.searchbar).subscribe(
             response =>{this.blogs = response;}
            );
  }
  getBlogByAnytime()
  {
  this.httpClientService.getAllBlogs().subscribe(
             response =>{this.blogs = response;}
            );
  }
  getBlogByLastHour()
  {
    this.httpClientService.getBlogByLastHour().subscribe(
               response =>{this.blogs = response;}
              );
  }
  getBlogByToday()
  {
      this.httpClientService.getBlogByToday().subscribe(
                 response =>{this.blogs = response;}
                );
  }
  getBlogByThisWeek()
  {
        this.httpClientService.getBlogByThisWeek().subscribe(
                   response =>{this.blogs = response;}
                  );
  }
  getBlogByThisMonth()
  {
        this.httpClientService.getBlogByThisMonth().subscribe(
                   response =>{this.blogs = response;}
                  );
  }
  getBlogByThisYear()
  {
        this.httpClientService.getBlogByThisYear().subscribe(
                   response =>{this.blogs = response;}
                  );
  }
  logout()
  {
    sessionStorage.removeItem('token');
    this.service.isLoggedIn(false);
    this.router.navigate(['login']);
  }
  goToMyProfile()
  {
    this.router.navigate(['/my-profile']);
  }
}
