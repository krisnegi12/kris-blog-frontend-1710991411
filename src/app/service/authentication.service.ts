import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient:HttpClient) { }
  authenticate(username:string,password:string)
  {
    const headers=new HttpHeaders({Authorization:'Basic '+btoa(username+':'+password)});
    return this.httpClient.get("http://localhost:2019/api/validateUser",{headers}).pipe(
    map(data => {
    sessionStorage.setItem('token',btoa(username+':'+password));
    }));
  }
}
