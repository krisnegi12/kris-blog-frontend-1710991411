import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {User} from '../User';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient:HttpClient) { }


   createUser(user)
   {
   return this.httpClient.post<User>('http://localhost:2019/api/createUser',user);
   }
        getUser()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/getuser',{headers});
        }
        editUser(json)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.put('http://localhost:2019/api/edituser',json,{headers});
        }
        getNewsFeed()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/newsfeed',{headers});
        }
        getAllBlogs()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/getAllBlogs',{headers});
        }
        getBlogById(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/'+id,{headers});
        }
        getUserById(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/user/'+id,{headers});
        }
        getUsersFollowing(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/usersfollowing/'+id,{headers});
        }
        getUserFollower(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/userfollower/'+id,{headers});
        }
        clickSubscribe(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/subscribe/'+id,{headers});
        }
        clickUnsubscribe(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/unsubscribe/'+id,{headers});
        }
        checksubscribe(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/checksubscribe/'+id,{headers});
        }
        addblog(json)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.post('http://localhost:2019/api/addblog',json,{headers});
        }
        editblog(id:Number,json)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.put('http://localhost:2019/api/editblog/'+id,json,{headers});
        }
        deleteblog(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/deleteblog/'+id,{headers});
        }
        getBlogBySearch(name:string)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/search/'+name,{headers});
        }
        getBlogByLastHour()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/searchbydate/lasthour',{headers});
        }
        getBlogByToday()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/searchbydate/today',{headers});
        }
        getBlogByThisWeek()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/searchbydate/thisweek',{headers});
        }
        getBlogByThisMonth()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/searchbydate/thismonth',{headers});
        }
        getBlogByThisYear()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/blog/searchbydate/thisyear',{headers});
        }
        getUserBlogs(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/userblogs/'+id,{headers});
        }
        addcomment(id:Number,content:string)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/addcomment/'+id+'/'+content,{headers});
        }
        deletecomment(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/deletecomment/'+id,{headers});
        }
        getBlogComments(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/getBlogComments/'+id,{headers});
        }
        clicklike(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/clicklike/'+id,{headers});
        }
        clickdislike(id:Number)
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/clickdislike/'+id,{headers});
        }
        getLikedByUser()
        {
         const token=sessionStorage.getItem('token');
         const headers=new HttpHeaders({Authorization:'Basic '+token});
         return this.httpClient.get('http://localhost:2019/api/getLikedByUser',{headers});
        }

}
