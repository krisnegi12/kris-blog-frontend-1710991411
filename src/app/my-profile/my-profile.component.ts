import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../service/http-client.service';
import {Router} from '@angular/router';
import {AppService} from '../service/app.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
user:any={"fullName":"",
     "userName":"",
     "email":"",
     "password":"",
     "role":"",
     "active":""};
following;
follower;
blogs;
  constructor(private httpClientService: HttpClientService,private router:Router,private service:AppService) { }

  ngOnInit() {
  this.httpClientService.getUser().subscribe(
  response =>{this.user=response;
  this.httpClientService.getUsersFollowing(this.user.id).subscribe(
                       response =>{this.following = response;}
                      );
          this.httpClientService.getUserFollower(this.user.id).subscribe(
                       response =>{this.follower = response;}
                      );
          this.httpClientService.getUserBlogs(this.user.id).subscribe(
                                 response =>{this.blogs = response;}
                                );
  }
  );
  }
editUser()
{
if(this.user.fullName==""||this.user.userName==""||this.user.email==""||this.user.password==""||this.user.role=="")
{
alert("Please fill all the credentials");
}
else{
let json={"fullName":this.user.fullName,
"userName":this.user.userName,
"email":this.user.email,
"password":this.user.password,
"role":this.user.role,
"active":this.user.active};
this.httpClientService.editUser(json).subscribe(
  response =>{this.user=response;alert("Profile successfully updated");sessionStorage.removeItem('token');this.service.isLoggedIn(false);this.router.navigate(['login']);}
  );
  }
}
goToBlogDetail(id)
{
      this.router.navigate(['/blog-detail'],{queryParams:{id:id}});
}
getProperDate(milli)
{
    var d=new Date(milli);
    return d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
}
}
