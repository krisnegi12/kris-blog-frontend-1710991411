import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.scss']
})
export class CreateBlogComponent implements OnInit {
name;
description;
content;
access=false;

  constructor(private httpClientService: HttpClientService) { }

  ngOnInit() {
  }
createBlog()
{
if(this.name==undefined||this.description==undefined||this.content==undefined)
{
alert("Please fill all the credentials");
}
else{
let json={
"name":this.name,
"img":"",
"date":0,
"description":this.description,
"content":this.content,
"access":this.access
};
this.httpClientService.addblog(json).subscribe(
response =>{console.log(response);alert("Blog successfully created");}
);
}
}
}
