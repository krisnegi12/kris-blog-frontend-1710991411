import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-update-blog',
  templateUrl: './update-blog.component.html',
  styleUrls: ['./update-blog.component.scss']
})
export class UpdateBlogComponent implements OnInit {
blogId;
blog:any={
"name":"",
"img":"",
"date":0,
"description":"",
"content":"",
"access":false
};

  constructor(private activatedRoute:ActivatedRoute,private httpClientService: HttpClientService) { }

  ngOnInit() {
  this.activatedRoute.queryParams.subscribe(params => {
      this.blogId=params.id;

      });
      this.httpClientService.getBlogById(this.blogId).subscribe(
             response =>{this.blog = response;}
            );
  }
  updateBlog()
  {
    if(this.blog.name==""||this.blog.description==""||this.blog.content=="")
    {
    alert("Please fill all the credentials");
    }
    else{
    let json={
    "name":this.blog.name,
    "img":this.blog.img,
    "date":this.blog.date,
    "description":this.blog.description,
    "content":this.blog.content,
    "access":this.blog.access
    }
    this.httpClientService.editblog(this.blogId,json).subscribe(
    response =>{console.log(response);alert("Blog successfully edited & saved");}
    );
    }
  }

}
