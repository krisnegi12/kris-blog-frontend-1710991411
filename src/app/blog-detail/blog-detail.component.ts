import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnInit {
blogId;
blog;
user;
checkUser;
checkModerator=false;
content;
comments;
liked:any=[];
currentstate;

  constructor(private activatedRoute:ActivatedRoute,private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit() {
  this.activatedRoute.queryParams.subscribe(params => {
    this.blogId=params.id;

    });
    this.httpClientService.getBlogById(this.blogId).subscribe(
           response =>{this.blog = response;
           this.httpClientService.getUser().subscribe(response=>{this.user=response;
               if(this.blog.user.id==this.user.id)
               {
               this.checkUser=true;
               }
               else
               {
               this.checkUser=false;
               }
               if(this.user.role=="moderator")
               {
               this.checkModerator=true;
               }
               });
           }
          );
    this.httpClientService.getBlogComments(this.blogId).subscribe(response=>{this.comments=response;});
    this.httpClientService.getLikedByUser().subscribe(response=>{this.liked=response;});
  }
  getProperDate(milli)
  {
        var d=new Date(milli);
        return d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
  }
  goToUserProfile(id)
  {
      this.router.navigate(['/user-profile'],{queryParams:{id:id}});
  }
  goToUpdateBlog(id)
  {
        this.router.navigate(['/update-blog'],{queryParams:{id:id}});
  }
  deleteBlog(id)
  {
        this.httpClientService.deleteblog(this.blogId).subscribe(
                   response =>{console.log(response);alert("Blog successfully deleted");this.router.navigate(['/view-blogs']);}
                  );
  }
  addcomment()
  {
  this.httpClientService.addcomment(this.blogId,this.content).subscribe(
                     response =>{console.log(response);alert("Comment successfully added");this.ngOnInit();}
                    );
  }
  deletecomment(id)
  {
    this.httpClientService.deletecomment(id).subscribe(
                       response =>{console.log(response);alert("Comment successfully deleted");this.ngOnInit();}
                      );
  }
  checkComment(c)
  {
   if(c.user.id==this.user.id)
   {
   return true;
   }
   else
   {
   return false;
   }
  }
  clicklike(id)
  {
  this.httpClientService.clicklike(id).subscribe(
                         response =>{console.log(response);alert("Comment liked");this.ngOnInit();}
                        );
  }
  clickdislike(id)
  {
    this.httpClientService.clickdislike(id).subscribe(
                           response =>{console.log(response);alert("Comment disliked");this.ngOnInit();}
                          );
  }
  checkliked(c)
  {
  const found=this.liked.find(function(value){
            return value.comments.id===c.id;
            });
            if(found)
            {
             if(found.likestate==true)
              this.currentstate="~Comment liked by you~";
             else
              this.currentstate="~Comment disliked by you~";
            return !found.likestate;
            }
            else
            {
            this.currentstate="";
            return true;
            }
  }

}
