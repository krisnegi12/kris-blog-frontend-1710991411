import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {HttpClientService} from '../service/http-client.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
userId;
user={};
following;
follower;
checksub;

  constructor(private activatedRoute:ActivatedRoute,private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit() {
  this.activatedRoute.queryParams.subscribe(params => {
      this.userId=params.id;

      });
      this.httpClientService.getUserById(this.userId).subscribe(
             response =>{this.user = response;}
            );
      this.httpClientService.getUsersFollowing(this.userId).subscribe(
                   response =>{this.following = response;}
                  );
      this.httpClientService.getUserFollower(this.userId).subscribe(
                   response =>{this.follower = response;}
                  );
      this.httpClientService.checksubscribe(this.userId).subscribe(
                         response =>{this.checksub = response;}
                        );
  }
  clickSubscribe()
  {
  this.httpClientService.clickSubscribe(this.userId).subscribe(
                     response =>{console.log(response);alert("User successfully subscribed");this.ngOnInit();}
                    );
  }
  clickUnsubscribe()
  {
    this.httpClientService.clickUnsubscribe(this.userId).subscribe(
                       response =>{console.log(response);alert("User successfully unsubscribed");this.ngOnInit();}
                      );
  }

}
