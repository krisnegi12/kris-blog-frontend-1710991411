import { Component, OnInit } from '@angular/core';
import {HttpClientService} from '../service/http-client.service';
import {User} from '../User';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
user:User=new User("","","","","","");
  constructor(private httpClientService: HttpClientService,private router:Router) { }

  ngOnInit() {
  }
createUser()
{
if(this.user.fullName==""||this.user.userName==""||this.user.email==""||this.user.password=="")
{
alert("Please fill all the credentials");
}
else{
this.httpClientService.createUser(this.user).subscribe(data =>{console.log(data);alert("User created successfully");this.router.navigate(['login']);});
}
}
goToLog()
{
this.router.navigate(['login']);
}
}
